const fs = require('fs');

var fetch = () => {
  try {
    var items = fs.readFileSync('data.json');
    return JSON.parse(items);
  } catch (e) {
    return [];
  }
};

var save = (items) => {
  fs.writeFileSync('data.json', JSON.stringify(items));
};

var addItem = (key, value) => {
  var items = fetch();
  var item = {
    key,
    value
  };
  var duplicateItems = items.filter((item) => item.key === key);

  if (duplicateItems.length === 0) {
    items.push(item);
    save(items);
    return true;
  }
    return false;
};

var listItems = () => {
  return fetch();
};

var getItem = (key) => {
  var items = fetch();
  var filteredItems = items.filter((item) => item.key === key);
  return filteredItems[0] || 'The key didn\'t match any item';
};

var removeItem = (key) => {
  var items = fetch();
  var filteredItems = items.filter((item) => item.key !== key);
  save(filteredItems);

  return items.length !== filteredItems.length;
};

var clearAll = () => {
    fs.writeFileSync('data.json', JSON.stringify([]));
    return true;
};

module.exports = {
  addItem,
  listItems,
  getItem,
  removeItem,
  clearAll
};
