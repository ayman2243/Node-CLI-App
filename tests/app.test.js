const fs = require('fs');
var nexpect = require('nexpect');


beforeEach(() => {
    fs.writeFileSync(`${__dirname}/../data.json`, JSON.stringify([{key: "item", value: "test"}]));
});

describe('test add item', () => {

    it('should view error message if the key or value is not defined', (done) =>{
        nexpect.spawn("node", ["app.js", "add"])
         .expect("Key & Value is required to store any item.")
         .run(function (err) {
           if (err) {
            throw err;
           }
           return done();
         });
    });

    it('should return true when the item added successfully', (done) =>{
        nexpect.spawn("node", ["app.js", "add", "test", "test"])
         .expect('true')
         .run(function (err) {
           if (!err) {
            return done();
           }
           throw err;
         });
    });
});

describe('test remove item', () => {

    it('should view error message if the key is not defined', (done) =>{
        nexpect.spawn("node", ["app.js", "remove"])
         .expect("Key is required to remove any item.")
         .run(function (err) {
           if (err) {
            throw err;
           }
           return done();
         });
    });

    it('should return true when the item removed successfully', (done) =>{
        nexpect.spawn("node", ["app.js", "remove", "item"])
         .expect('true')
         .run(function (err) {
           if (!err) {
            return done();
           }
           throw err;
         });
    });
});

describe('test get item', () => {

    it('should view error message if the key is not defined', (done) =>{
        nexpect.spawn("node", ["app.js", "get"])
         .expect("Key is required to view any item.")
         .run(function (err) {
           if (err) {
            throw err;
           }
           return done();
         });
    });

    it('should return the object', (done) =>{
        nexpect.spawn("node", ["app.js", "get", "item"])
         .expect('{"key":"item","value":"test"}')
         .run(function (err) {
           if (!err) {
            return done();
           }
           throw err;
         });
    });
});

describe('test list/clear all', () => {

    it('should return array of items', (done) =>{
        nexpect.spawn("node", ["app.js", "list"])
         .expect('[{"key":"item","value":"test"}]')
         .run(function (err) {
           if (!err) {
            return done();
           }
           throw err;
         });
    });

    it('should return true', (done) =>{
        nexpect.spawn("node", ["app.js", "clear"])
         .expect('true')
         .run(function (err) {
           if (!err) {
            return done();
           }
           throw err;
         });
    });
});


