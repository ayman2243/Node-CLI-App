/* Node.js CLI App */

'use strict';
const fs = require('fs').promises;

const store = require('./store.js');

var command = process.argv[2];
var key = process.argv[3];
var value = process.argv[4];

switch(command){
    case "add":
        if(!key || !value)
            console.log('Key & Value is required to store any item.');
        else
            console.log(store.addItem(key, value));
        break;

    case "list":
        console.log(JSON.stringify(store.listItems(), undefined));
        break;

    case "get":
        if(!key)
            console.log('Key is required to view any item.');
        else
            console.log(JSON.stringify(store.getItem(key), undefined));   
        break;
    case "remove":
        if(!key)
            console.log('Key is required to remove any item.');
        else
            console.log(store.removeItem(key));
        break;

    case "clear":
        console.log(store.clearAll());    
        break;
    default:
        console.log('Command not recognized');
}
